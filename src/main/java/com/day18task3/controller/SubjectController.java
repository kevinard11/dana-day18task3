package com.day18task3.controller;

import com.day18task3.model.Student;
import com.day18task3.model.Subject;
import com.day18task3.repository.SubjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subjects")
public class SubjectController {
    // Spring Boot Logger
    public static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    SubjectRepository subjectRepository;

    // -----------------Register Subject----------------------
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<Subject> insertSubject(@RequestBody Subject subject) {
        log.info("Adding...{}", subject.getName());
        int result = subjectRepository.insert(subject);

        if (result > 0) {
            log.info("Add {} success!", subject.getName());
            return new ResponseEntity<Subject>(subject, HttpStatus.CREATED);
        } else {
            log.info("Register {} failed!", subject.getName());
            return new ResponseEntity<Subject>(subject, HttpStatus.CONFLICT);
        }
    }

    // -----------------Get Subjects Count-----------------------
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity<?> getCount() {
        log.info("Counting Subjects...");
        int countResult = subjectRepository.count();
        log.info("[COUNT] Total Subjects: {}", countResult);
        return new ResponseEntity<Integer>(countResult, HttpStatus.OK);
    }

    // ------------------Get All Subjects-----------------------
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Subject>> getAllSubjects() {
        log.info("Finding All Subjects...");
        List<Subject> subjects = subjectRepository.findAll();
        log.info("[FIND_ALL] {}", subjects);
        return new ResponseEntity<List<Subject>>(subjects, HttpStatus.OK);
    }

    // -----------------Get Student by ID-----------------------
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Subject> getSubjecttById(@PathVariable("id") int id) {
        log.info("[FIND_BY_ID] :{}", id);
        Subject subject = subjectRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        log.info("{}", subject);
        return new ResponseEntity<Subject>(subject, HttpStatus.OK);
    }

    // -------------------Delete Subject------------------------
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteSubjectById(@PathVariable("id") int id) {
        log.info("[DELETE] :{}", id);
        int result = subjectRepository.deleteById(id);
        log.info("rows affected: {}", result);
        return new ResponseEntity<String>("success", HttpStatus.OK);
    }
}
