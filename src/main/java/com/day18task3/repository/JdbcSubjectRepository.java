package com.day18task3.repository;

import com.day18task3.model.Student;
import com.day18task3.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class JdbcSubjectRepository implements SubjectRepository{

    // Spring Boot will create and configure DataSource and JdbcTemplate
    // To use it, just @Autowired
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public int count() {
        return jdbcTemplate
                .queryForObject("select count(*) from subjects", Integer.class);
    }

    @Override
    public int insert(Subject subject) {
        return jdbcTemplate.update(
                "insert into subjects (name) values(?)",
                subject.getName());
    }

    @Override
    public int deleteById(int id) {
        return jdbcTemplate.update(
                "delete from subjects where id = ?",
                id);
    }

    @Override
    public List<Subject> findAll() {
        return jdbcTemplate.query(
                "select * from subjects",
                (rs, rowNum) ->
                        new Subject(
                                rs.getInt("id"),
                                rs.getString("name")
                        )
        );
    }

    @Override
    public Optional<Subject> findById(int id) {
        return jdbcTemplate.queryForObject(
                "select * from subjects where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        Optional.of(new Subject(
                                rs.getInt("id"),
                                rs.getString("name")
                        ))
        );
    }
}
