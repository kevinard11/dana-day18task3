package com.day18task3.repository;

import com.day18task3.model.Student;

import java.util.List;
import java.util.Optional;

public interface StudentRepository {
    int count();
    int insert(Student student);
    int updateAttendance(int id, String attendance);
    int deleteById(int id);

    List<Student> findAll();
    Optional<Student> findById(int id);
}
