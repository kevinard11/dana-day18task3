package com.day18task3.repository;

import com.day18task3.model.Subject;

import java.util.List;
import java.util.Optional;

public interface SubjectRepository {
    int count();
    int insert(Subject subject);
    int deleteById(int id);

    List<Subject> findAll();
    Optional<Subject> findById(int id);
}
