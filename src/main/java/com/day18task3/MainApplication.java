package com.day18task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication(scanBasePackages={"com.day18task3"})// same as @Configuration @EnableAutoConfiguration @ComponentScan combined
public class MainApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(MainApplication.class);

    // Spring Boot will create and configure DataSource and JdbcTemplate
    // To use it, just @Autowired
    @Autowired
    JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("Starting Application...");
        log.info("Creating Database if not exists...");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS students(" +
                "id int AUTO_INCREMENT, name VARCHAR(255), address VARCHAR(255), major VARCHAR(255), attendance DATE, PRIMARY KEY (id))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS subjects(" +
                "id int AUTO_INCREMENT, name VARCHAR(255), PRIMARY KEY (id))");

        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS studentsubject(" +
                "id int AUTO_INCREMENT, studentid int, subjectid int, attendance DATE, score int, PRIMARY KEY (id), FOREIGN KEY (studentid) REFERENCES students(id), FOREIGN KEY (subjectid) REFERENCES subjects(id))");

    }
}